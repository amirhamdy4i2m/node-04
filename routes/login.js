const { User } = require("../models/user");
const Joi = require("joi");
const config = require("config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send("Login...");
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("Invalid username or password.");

  const validPassword = bcrypt.compare(user.password, req.body.password);
  if (!validPassword)
    return res.status(400).send("Invalid username or password.");

  const token = user.generateToken();

  res.send(token);
});

function validate(req) {
  const schema = {
    email: Joi.string()
      .min(5)
      .max(200)
      .required()
      .email(),
    password: Joi.string()
      .min(5)
      .max(1024)
      .required()
  };

  return Joi.validate(req, schema);
}
module.exports = router;
