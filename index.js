const { User } = require("./models/user");
const config = require("config");
const homeRouter = require("./routes/home");
const usersRouter = require("./routes/users");
const registerRouter = require("./routes/register");
const loginRouter = require("./routes/login");

const mongoose = require("mongoose");
const express = require("express");

const app = express();

if (!config.get("jwtPrivateKey")) {
  console.error("Error: jwtPrivateKey is not defined.");
  process.exit(1);
}

mongoose
  .connect("mongodb://localhost/node-04", { useNewUrlParser: true })
  .then(() => console.log("connected to database..."))
  .catch(err => console.error("Error while connecting to database...", err));

app.use(express.json());
app.use("/", homeRouter);
app.use("/users", usersRouter);
app.use("/register", registerRouter);
app.use("/login", loginRouter);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Listening on port ${port}...`);
});
